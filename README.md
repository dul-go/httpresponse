# Database Utilities (dul-go)

## Installing to local Go environment

Run `go get gitlab.oit.duke.edu/dul-go/httpresponse` to download the module to your 
local setup.  
  
If that fails to work (and it might), do the following:
* create a GitLab personal access token with `read_repository` and `read_api` permission scopes
* For Linux (and likely MacOS) users, add a `.netrc` file at your home directory, with the following content:  
  
```sh
machine machine gitlab.oit.duke.edu
  login git@gitlab.oit.duke.edu
  password <your-newly-created-personal-access-token>
```
### DEPRECATED (no longer works)
See this page:
https://edenmal.moe/post/2017/Golang-go-get-from-Gitlab/

```sh
git config --global url."git@gitlab.oit.duke.edu:".insteadOf "https://gitlab.oit.duke.edu/"

go get gitlab.oit.duke.edu/dul-go/httpresponse
```

