package httpresponse

import (
	"net/http"
)

// PageDocument returns PageDocument
// We'll keep the a page's "title", and "body" (rendered page contents)
// in this structure.
//
// Implements io.Writer
type PageDocument struct {
	// The title that can appear at the top of a browser's page
	// and/or the top of the page contents
	Title string

	// The rendered content (for HTML), or otherwise (JSON, etc)
	// relative to this page
	Body []byte
}

func (p *PageDocument) Write(buf []byte) (int, error) {
	p.Body = append(p.Body, buf...)
	return 0, nil
}

// PageController returns PageController
// Implementers will process a request, likely, by:
// - gathering data (from DB or other source)
// - write page content (rendered templates, or otherwise) to
//   a PageDocument's Body field
// ... and return a pointer to a PageDocument, or error
type PageController func(w http.ResponseWriter,
	r *http.Request,
	contextData map[string]interface{}) (*PageDocument, error)
